# Writing #

This is a collection of writing that i have been doing, or well, might be doing in the future.

The repository consists of Emacs Org mode files. For more information on Org
mode see https://orgmode.org/  or the info page inside emacs.

You can generate PDF or even EPUB files from the org files inside of emacs as
long as you have the suitable tool chain installed.  On Mac that includes a
working version of Homebrew and Emacs, Inside the settings folder you can find a
.emacs file listing the packages i happen to use. The homebrew packages i have
installed can be seen in the homebrew.list file, although most are not
nessecary. Note that i compile my own version of Emacs on Mac (with GUI) that I
use and not the homebrew version. PDF generation relies on a working LaTeX
environment. 

You can read the .org file just as any markup file, although the table of contents, 
list of figures, index and references will not render as well as in a PDF since those rely on 
LaTeX processing. 

The references are stored in .bib files and contain references to additional
writing in BibTeX format that is used as such by the PDF generator. 

All text and images are available under a Creative Commons license (Creative
Commons license Attribution-NonCommercial 4.0 International - CC BY-NC, see 
https://creativecommons.org/licenses/by-nc/4.0/ and thanks, I apropriated your image).



## Thoughts About Information Technology ##

This is a writing project that i starded early 2022 to collect my thinking on
the many aspects of information technology theory and practice that floats
around. The book or whatever it will turn out to is to be considered a work in
progress. It is not ready. I have not done any editing and everything is written
as a free flow of ideas typed out on my keyboard. There is text for some
chapters, while others are just ideas marked with a headline. The headings may
change as well as the structure. Once the text is a bit more evolved I will
publish a PDF somewhere and link it here, but currently there is only source org
text files.

The current state of this work April 2023 is that it is still a work in progress.
At the end of August 2022 is that the book was a bit 
over 100 pages if rendered in PDF. Now in April there are close to 200 pages over all.
There are still many chapters missing. There is some duplication of concepts and text. 
Some ideas are just fleshed out without proper discussion.  Not 
much editing has been done and the writing procedes very much without use of
either backspace, kill or delete being used. 


