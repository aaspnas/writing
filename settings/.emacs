;; Add the below lines *at the beginning* of your .emacs.
(require 'package)
(setq package-archives
      '(("elpa"     . "https://elpa.gnu.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("melpa"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("melpa-stable" . 10)
        ("elpa"     . 5)
        ("melpa"        . 0)))
(package-initialize)
(package-refresh-contents)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

(setq gc-cons-threshold 50000000)
(setq gnutls-min-prime-bits 4096)

(when window-system (set-frame-size (selected-frame) 200 65))

(setq visible-bell t)

(use-package dash-at-point
  :ensure t
  :bind ("C-c d d" . dash-at-point-with-docset)
  :config
  (add-to-list 'dash-at-point-mode-alist '(clojure-mode . "clojure"))
  (add-to-list 'dash-at-point-mode-alist '(java-mode . "java"))
  (add-to-list 'dash-at-point-mode-alist '(javascript-mode . "javascript"))
  (add-to-list 'dash-at-point-mode-alist '(perl-mode . "perl"))
  (add-to-list 'dash-at-point-mode-alist '(c-mode . "c"))
  (add-to-list 'dash-at-point-mode-alist '(c++-mode . "c++"))
  (add-to-list 'dash-at-point-mode-alist '(R-mode . "R"))  
  (add-to-list 'dash-at-point-mode-alist '(ruby-mode . "ruby"))
  (add-to-list 'dash-at-point-mode-alist '(python-mode . "python3"))
  (add-to-list 'dash-at-point-mode-alist '(sh-mode . "bash"))
  (add-to-list 'dash-at-point-mode-alist '(emacs-lisp-mode . "elisp")))

(when (window-system)
  (tool-bar-mode 0)              
  (when (fboundp 'horizontal-scroll-bar-mode)
    (horizontal-scroll-bar-mode -1))
  (scroll-bar-mode -1))           

(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :init
  (global-undo-tree-mode 1)
  :config
  (defalias 'redo 'undo-tree-redo)
  :bind (("C-z" . undo)
         ("C-S-z" . redo)))

(use-package ace-window
  :ensure t
  :init
    (setq aw-keys '(?a ?s ?d ?f ?j ?k ?l ?o))
    (global-set-key (kbd "C-x o") 'ace-window)
  :diminish ace-window-mode)

(setq inhibit-startup-message t)
(setq-default fill-column 80)

(global-font-lock-mode 1)

(use-package org
  :ensure t        ; But it comes with Emacs now!?
  :init
  ;;(font-lock-add-keywords 'org-mode
  ;;  '(("^ +\\([-*]\\) "
  ;;      (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  (setq org-use-speed-commands t
        org-return-follows-link t
        ;; org-hide-emphasis-markers t
        org-completion-use-ido t
        org-outline-path-complete-in-steps nil
        org-src-fontify-natively t   ;; Pretty code blocks
        org-src-tab-acts-natively t
        org-confirm-babel-evaluate nil
        org-todo-keywords '((sequence "TODO(t)" "DOING(g)" "|" "DONE(d)")
                            (sequence "|" "CANCELED(c)")))
  (setq org-pretty-entities t
      org-ellipsis " ▹ " ;; folding symbol
      org-hide-emphasis-markers t
      org-fontify-whole-heading-line t
      org-fontify-done-headline t
      org-fontify-quote-and-verse-blocks t
      ;; show actually italicized text instead of /italicized text/
      org-log-done t
      org-log-into-drawer t
      org-return-follows-link t
      org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      )
  (add-to-list 'auto-mode-alist '("\\.txt\\'" . org-mode))
  (add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode))   ;; Journal entries
  (add-hook 'org-mode-hook 'yas-minor-mode-on)
  :bind (("C-c l" . org-store-link)
         ("C-c c" . org-capture)
         ("C-M-|" . indent-rigidly))
  :config
  (define-key org-mode-map (kbd "M-C-n") 'org-end-of-item-list)
  (define-key org-mode-map (kbd "M-C-p") 'org-beginning-of-item-list)
  (define-key org-mode-map (kbd "M-C-u") 'outline-up-heading)
  (define-key org-mode-map (kbd "M-C-w") 'org-table-copy-region)
  (define-key org-mode-map (kbd "M-C-y") 'org-table-paste-rectangle)

  (define-key org-mode-map [remap org-return] (lambda () (interactive)
                                                (if (org-in-src-block-p)
                                                    (org-return)
                                                  (org-return-indent)))))


(setq default-major-mode 'org-mode)


(add-hook 'org-mode-hook 'flyspell-mode)
(setq flyspell-mode-line-string nil)
(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook 'word-count-mode)
(add-hook 'org-mode-hook 'org-indent-mode)

(setq mastodon-instance-url "https://toot.cafe"
      mastodon-active-user "aaspnas"
      mastodon-token-file "~/.toot.cafe")

(use-package vertico
  :bind (:map vertico-map
         ("C-j" . vertico-next)
         ("C-k" . vertico-previous)
         ("C-f" . vertico-exit)
         :map minibuffer-local-map
         ("M-h" . dw/minibuffer-backward-kill))
  :custom
  (vertico-cycle t)
  :custom-face
  (vertico-current ((t (:background "#3a3f5a"))))
  :init
  (vertico-mode))

(use-package marginalia
  :after vertico
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

(setq langtool-java-bin "/usr/bin/java")
;;(setq langtool-language-tool-jar"/opt/homebrew/Cellar/languagetool/5.7/libexec/languagetool-commandline.jar")
(setq langtool-language-tool-jar (eshell-command "find /opt/homebrew/Cellar/languagetool -name languagetool-commandline.jar"))
(setq langtool-default-language "en-US")
(setq langtool-mother-tongue "en")



(recentf-mode 1)
(save-place-mode 1)

(setq use-dialog-box nil)
(setq global-auto-refert-non-file-buffers t)

;; change "~/elisp/" as appropiate
(setq load-path (cons "~/elisp" load-path))

 (autoload 'word-count-mode "word-count"
          "Minor mode to count words." t nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (js         . t)
   (perl       . t)
   (python     . t)
   (emacs-lisp . t)
   (powershell . t)))

(eval-after-load 'org-src
  '(define-key org-src-mode-map
     (kbd "C-x C-s") #'org-edit-src-exit))

(setq org-confirm-babel-evaluate nil)
 
(setq org-src-fontify-natively t)
(setq org-src-tab-acts-natively t)

(blink-cursor-mode -1)
(defalias 'yes-or-no-p 'y-or-n-p)

(setq tags-table-list
      '("~/elisp" ))
 
;; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; (package-initialize)


(global-set-key (kbd "C-x k") 'kill-this-buffer)
(global-set-key (kbd "C-x K") 'kill-buffer)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(ispell-silently-savep t)
 '(ns-right-alternate-modifier 'none)
 '(org-agenda-loop-over-headlines-in-active-region nil)
 '(org-bibtex-autogen-keys t)
 '(org-export-with-section-numbers t)
 '(org-latex-remove-logfiles nil)
 '(org-pandoc-epub-rights "Copyright 2022 Anders Aspnäs aaspnas@gmail.com CC-BY-NC ")
 '(package-selected-packages
   '(perspective extempore-mode projectile-speedbar projectile-variable pretty-speedbar use-package workgroups idomenu ac-etags company-ctags ctags-update company-quickhelp company-quickhelp-terminal company-ansible company-bibtex company-math company-org-block company-wordfreq ag wrap-region dash-alfred dash-at-point dash-docs friendly-tramp-path ibuffer-tramp ssh-deploy tramp tramp-auto-auth tramp-term tramp-theme osx-browse osx-lib osx-org-clock-menubar osx-plist osx-pseudo-daemon osx-trash poly-R poly-ansible poly-org langtool apache-mode auto-complete-clang-async blamer clang-capf company-c-headers company-shell company-web cpputils-cmake csv csv-mode diminish emr epresent esh-autosuggest eshell-vterm ess-R-data-view ess-r-insert-obj ess-smart-equals ess-smart-underscore ess-view ess-view-data feature-mode flycheck-cstyle flycheck-irony flycheck-yamllint fxrd-mode gherkin-mode gscholar-bibtex js3-mode ob-blockdiag ob-diagrams ob-ipython ob-powershell org-babel-eval-in-repl org-present osx-clipboard osx-dictionary pickle smart-semicolon smartparens smog spaces splitter srefactor test-c tomatinho wordsmith-mode writegood-mode writeroom-mode yagist embark embark-consult marginalia vertico vertico-posframe org-review org-zettelkasten org-roam-timestamps org-roam-ui company-plsense plsense plsense-direx ac-clang ac-ispell auto-auto-indent c-eldoc cucumber-goto-step dired-dups dired-sidebar dired-subtree ecukes flyspell-correct-ivy gist git-blamed highlight lang-refactor-perl languagetool org-modern orgnav ox-mediawiki ox-minutes ox-pandoc save-visited-files spdx vterm walkman xmlunicode xref-js2 xref-rst svg ebib ansi shut-up epl commander ac-c-headers ansible ansible-doc ansible-vault ant arxiv-mode auto-complete auto-complete-c-headers auto-dictionary auto-org-md autobookmarks autobuild bash-completion basic-c-compile bats-mode better-defaults better-shell biblio biblio-bibsonomy biblio-core bibretrieve bibtex-completion bibtex-utils bpe bpftrace-mode c-eval calfw-ical calfw-org call-graph chronos citar clang-format clang-format+ clear-text cmake-mode countdown crontab-mode cssh date-at-point decide dired-git dired-open docker docker-api docker-cli docker-compose-mode dockerfile-mode dokuwiki dokuwiki-mode dtrace-script-mode egg emacsql-psql empos ereader find-dupes-dired flex-autopair flex-compile flycheck flycheck-clang-tidy flycheck-dtrace flycheck-golangci-lint flycheck-gradle flycheck-ini-pyinilint flycheck-pycheckers flymake-gradle flymake-perlcritic flymake-shell flymake-shellcheck flymake-yamllint flymd flyparens flyspell-correct flyspell-correct-popup flyspell-popup format-all format-sql format-table git git-auto-commit-mode git-command git-commit git-lens git-messenger git-modes git-timemachine git-walktree gitconfig github-clone github-pullrequest github-review gitignore-snippets gnuplot gnuplot-mode go-mode gradle-mode helm helm-R helm-bibtex helm-bind-key helm-chrome helm-chrome-control helm-chrome-history helm-descbinds helm-describe-modes helm-dictionary helm-directory helm-file-preview helm-flycheck helm-flymake helm-flyspell helm-frame helm-git helm-git-files helm-git-grep helm-helm-commands helm-ispell helm-make helm-org helm-org-recent-headings helm-osx-app helm-perldoc helm-shell-history helm-sql-connect helm-switch-shell helm-switch-to-repl helm-tail helm-taskswitch helm-themes helm-unicode http-twiddle httprepl ibuffer-git imake indent-control indent-guide indent-info indent-lint indent-tools indicators insert-shebang irony itail ivy-bibtex ivy-todo java-imports jdee jq-format jq-mode jquery-doc js-auto-beautify js-auto-format-mode js-format js-import json-navigator json-par json-process-client json-reformat jsonl jtags jupyter jvm-mode kanban latex-extra latex-math-preview latex-pretty-symbols latex-preview-pane latex-unicode-math-mode latexdiff magit magit-org-todos markdown-mode markdown-preview-eww markdown-preview-mode markdown-toc markup mastodon most-used-words mvn nanowrimo nexus nginx-mode nov npm npm-mode nvm orca org-ac org-appear org-attach-screenshot org-auto-expand org-autolist org-beautify-theme org-brain org-bullets org-capture-pop-frame org-cliplink org-context org-dashboard org-doing org-download org-easy-img-insert org-edit-latex org-elp org-inline-pdf org-journal org-journal-list org-journal-tags org-kanban org-link-beautify org-linkotron org-listcruncher org-make-toc org-mind-map org-multi-wiki org-notebook org-noter org-noter-pdftools org-onenote org-pdftools org-pomodoro org-pretty-tags org-radiobutton org-recur org-ref org-ref-prettify org-roam org-roam-bibtex org-runbook org-scrum org-sidebar org-sql org-starter org-super-agenda org-superstar org-table-comment org-table-sticky-header org-tag-beautify org-tracktable org-tree-slide org-wc orgtbl-join ox-epub ox-html5slide ox-tiddly pallet paradox parsebib password-generator pg pgdevenv poly-markdown pomidor pomm pomodoro powershell prettier prettier-js processing-mode processing-snippets projectile py-test pydoc pylint pytest-pdb-break python-cell python-django python-docstring python-info python-insert-docstring python-mode python-pytest python-test redacted sqlformat sqlup-mode ssh ssh-config-mode suomalainen-kalenteri sv-kalender-namnsdagar synonymous synosaurus term+ term+mux term-manager terminal-here unidecode web web-beautify xml+ xml-format yaml yaml-imenu yaml-mode valign realgud realgud-ipdb realgud-jdb realgud-lldb realgud-node-debug realgud-node-inspect modus-operandi-theme modus-themes modus-vivendi-theme multi-mode multishell captain auctex auto-correct diff-hl diffview dired-git-info exwm gited js2-mode json-mode jsonrpc org org-edna org-real org-remark org-transclusion org-translate python sed-mode shelisp shell-command+ smart-yank soap-client sql-indent test-simple undo-tree which-key xclip xref)))

(require 'pretty-speedbar)
(setq pretty-speedbar-font "Font Awesome 6 Free Solid")

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Liberation Mono" :foundry "1ASC" :slant normal :weight normal :height 150 :width normal))))
 '(ein:basecell-input-area-face ((t (:extend t :background "gray12"))))
 '(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face :foreground "white"))))
 '(font-lock-comment-face ((t (:background "gray85" :foreground "black"))))
 '(font-lock-doc-face ((t (:background "gray70" :foreground "black"))))
 '(vertico-current ((t (:background "#3a3f5a")))))


(use-package wrap-region
  :ensure   t
  :config
  (wrap-region-global-mode t)
  (wrap-region-add-wrappers
   '(("(" ")")
     ("[" "]")
     ("{" "}")
     ("<" ">")
     ("'" "'")
     ("\"" "\"")
     ("‘" "’"   "q")
     ("“" "”"   "Q")
     ("*" "*"   "b"   org-mode)                 ; bolden
     ("*" "*"   "*"   org-mode)                 ; bolden
     ("/" "/"   "i"   org-mode)                 ; italics
     ("/" "/"   "/"   org-mode)                 ; italics
     ("~" "~"   "c"   org-mode)                 ; code
     ("~" "~"   "~"   org-mode)                 ; code
     ("=" "="   "v"   org-mode)                 ; verbatim
     ("=" "="   "="   org-mode)                 ; verbatim
     ("_" "_"   "u" '(org-mode markdown-mode))  ; underline
     ("**" "**" "b"   markdown-mode)            ; bolden
     ("*" "*"   "i"   markdown-mode)            ; italics
     ("`" "`"   "c" '(markdown-mode ruby-mode)) ; code
     ("`" "'"   "c"   lisp-mode)                ; code
     ))
  :diminish wrap-region-mode)

(defun surround (start end txt)
  "Wrap region with textual markers.

 Without active region (START and END), use the current 'symbol /
word' at point instead of TXT.

Useful for wrapping parens and angle-brackets to also
insert the matching closing symbol.

This function also supports some `org-mode' wrappers:

  - `#s` wraps the region in a source code block
  - `#e` wraps it in an example block
  - `#q` wraps it in an quote block"
  (interactive "r\nsEnter text to surround: " start end txt)

  ;; If the region is not active, we use the 'thing-at-point' function
  ;; to get a "symbol" (often a variable or a single word in text),
  ;; and use that as our region.

  (if (not (region-active-p))
      (let ((new-region (bounds-of-thing-at-point 'symbol)))
        (setq start (car new-region))
        (setq end (cdr new-region))))

  ;; We create a table of "odd balls" where the front and the end are
  ;; not the same string.
  (let* ((s-table '(("#e" . ("#+BEGIN_EXAMPLE\n" "\n#+END_EXAMPLE") )
                    ("#s" . ("#+BEGIN_SRC \n"    "\n#+END_SRC") )
                    ("#q" . ("#+BEGIN_QUOTE\n"   "\n#+END_QUOTE"))
                    ("<"  . ("<" ">"))
                    ("("  . ("(" ")"))
                    ("{"  . ("{" "}"))
                    ("["  . ("[" "]"))))    ; Why yes, we'll add more
         (s-pair (assoc-default txt s-table)))

    ;; If txt doesn't match a table entry, then the pair will just be
    ;; the text for both the front and the back...
    (unless s-pair
      (setq s-pair (list txt txt)))

    (save-excursion
      (narrow-to-region start end)
      (goto-char (point-min))
      (insert (car s-pair))
      (goto-char (point-max))
      (insert (cadr s-pair))
      (widen))))

(global-set-key (kbd "C-+") 'surround)


(defun surround-text-with (surr-str)
  "Return an interactive function that when called, surrounds region (or word) with string, SURR-STR."
  (lexical-let ((text surr-str))
      (lambda ()
        (interactive)
        (if (region-active-p)
            (surround (region-beginning) (region-end) text)
          (surround nil nil text)))))

(use-package winner
  :ensure t
  :init (winner-mode 1))

(use-package ag
  :ensure    t
  :commands  ag
  :init      (setq ag-highlight-search t)
  :config    (add-to-list 'ag-arguments "--word-regexp"))


(use-package company
  :ensure t
  :init
  (setq company-dabbrev-ignore-case t
        company-show-numbers t)
  (add-hook 'after-init-hook 'global-company-mode)
  :config
  (add-to-list 'company-backends 'company-math-symbols-unicode)
  :bind ("C-:" . company-complete)  ; In case I don't want to wait
  :diminish company-mode)

(use-package company-quickhelp
  :ensure t
  :config
  (company-quickhelp-mode 1))

(use-package ctags-update
  :ensure t
  :config
  (add-hook 'prog-mode-hook  'turn-on-ctags-auto-update-mode)
  :diminish ctags-auto-update-mode)
 
(defun my-c-mode-hook ()
  (setq c-basic-offset 4)
  (c-set-offset 'substatement-open 0)   ; Curly braces alignment
  (c-set-offset 'case-label 4))         ; Switch case statements alignment

(add-hook 'c-mode-hook 'my-c-mode-hook)
(add-hook 'java-mode-hook 'my-c-mode-hook)


(use-package workgroups
  :ensure t
  :diminish workgroups-mode
  :config
  (setq wg-prefix-key (kbd "C-c a"))
  (workgroups-mode 1)
  (wg-load "~/.emacs.d/workgroups"))
